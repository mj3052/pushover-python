"""
The MIT License (MIT)

Copyright (c) 2013 Mathias Jensen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys
import requests
debug = False

def pushMessage(token, user, message, time='', priority='0', retry='',expire='', url='', url_title='', sound=''):
    # Init required parameters and some optional parameters
    parameters = {'token': token, 'user': user, 'message': message, 'priority': priority}
    
    # If time has been set, appen timestap to parameters
    if time != '':
        parameters.update({'timestamp':time})

    # If priority is important set other required parameters
    if priority == '2':
        parameters.update({'retry': retry, 'expire': expire});
    else:
        # Else check if retry or expire has been set, and add those if needed
        if retry != '':
            parameters.update({'retry': retry});

        if expire != '':
            parameters.update({'expire': expire});

    # If a url is set, append
    if url != '':
        parameters.update({'url': url});
    # Give the URL a title if present
    if url_title != '':
        parameters.update({'url_title': url_title});

    # Possible sounds: pushover, bike, bugle, cashregister, classical, cosmic, falling, gamelan, incoming,
    # intermission, magic, mechanical, pianobar, siren, spacealarm, tugboat, alien, climb, persistent, echo, updown, none
    # If sound is defined, append it
    if sound !='':
        parameters.update({'sound': sound})
    
    # Post the request
    r = requests.post('https://api.pushover.net/1/messages.json', params=parameters)

    # If debug is on, log the response body
    if debug:
        print(r.content)

    response = r.json()
    return response
